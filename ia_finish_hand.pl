%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% IA : Finish hand absolutely

:- ensure_loaded('domino_utils.pl').
:- ensure_loaded('hand_utils.pl').

hand_without_jokers([], []).
hand_without_jokers([[0,_]|Hand_tail], Clean_hand) :- hand_without_jokers(Hand_tail, Clean_hand).
hand_without_jokers([[_,0]|Hand_tail], Clean_hand) :- hand_without_jokers(Hand_tail, Clean_hand).
hand_without_jokers([Hand_head|Hand_tail], Clean_hand) :- hand_without_jokers(Hand_tail, Clean_hand_tmp),
														 append(Clean_hand_tmp, [Hand_head], Clean_hand),!. 



%%%% Ajoute un élément Domino dans chaque sous-liste de la liste [Head_chain|Tail_chain]
%%%% Signature : append_to_sublists(Liste de chaînes de dominos, 
%%%%								Domino à ajouter, 
%%%%								Liste de chaînes de dominos auxquelles ont été ajoutés l'élément Domino)
append_to_sublists([Head_chain|Tail_chain], Domino, [Head_appended_chain|Tail_appended_chain]) :-
	is_list(Head_chain),
	append(Head_chain, [Domino], Head_appended_chain),
	append_to_sublists(Tail_chain, Domino, Tail_appended_chain)
	.

% Pour une liste de chaînes vide, on ajoute simplement l'élément seul dans une chaîne
append_to_sublists([], A, [[A]]).
	


%%%% Signature : (Dominos dans la main du joueur, 
%%%%			  Compteur pour le parcours des dominos, 
%%%%			  Extrémité de la chaîne qu'on souhaite prolonger, 
%%%%			  Liste contenant les chaînes jouables)	
get_domino_chains(Player_hand, Counter, Chain_initiator, Chains) :-
	% On récupère l'élément à l'index Counter. S'il y en a pas, prédicat faux.
	nth0(Counter, Player_hand, Domino),
	% On récupère le nombre accolé à l'initiateur de chaîne dans le domino. 
	% Si l'initiateur de chaîne n'est pas présent, le prédicat est faux.
	domino_get_numbers(Domino, Chain_initiator, Last_number), 
	% On récupère la main, sans le domino en cours d'étude.
	delete(Player_hand, Domino, Player_hand_wht_domino),
	% On applique encore l'algorithme, avec la nouvelle main sans le domino étudié,
	% avec comme initiateur de chaîne Last_number (nombre accolé du domino).
	get_domino_chains(Player_hand_wht_domino, 0, Last_number, Smaller_chains),
	% La ligne ci-dessus nous permet de connaître les sous-chaînes auxquelles on peut
	% accoler le domino en cours d'étude. (ne pas oublier l'idée principale : on ajoute
	% le domino lorsque l'on remonte l'arbre...)
	append_to_sublists(Smaller_chains, Domino, Tmp_chains),
	% On continue à tester les autres possibilités en explorant les autres dominos de la main.
	continue_chaining(Player_hand, Counter, Chain_initiator, Tmp_chains2),
	append(Tmp_chains, Tmp_chains2, Chains)
	.

get_domino_chains(Player_hand, Counter, Chain_initiator, Chains) :-
	continue_chaining(Player_hand, Counter, Chain_initiator, Chains).
	
% Quand la main est vide, on ne peut trouver aucune chaîne de dominos.
get_domino_chains([], _, _, []).
	
continue_chaining([], _, _, []).	
	
% Permet de parcourir la main. Si on a atteint la limite de la liste, on s'arrête.	
continue_chaining(Player_hand, Counter, Chain_initiator, Chains) :-
	Incremented_counter is Counter + 1,
	proper_length(Player_hand, Length),
	Incremented_counter < Length,
	get_domino_chains(Player_hand, Incremented_counter, Chain_initiator, Chains)
	.
	
continue_chaining(Player_hand, Counter, _, []) :-
	Incremented_counter is Counter + 1,
	proper_length(Player_hand, Length),
	Incremented_counter == Length
	.

%%%% Permet, dans une liste composée de sous-listes, d'enlever celles qui sont incluses,
%%%% à condition que les sous-listes soient triées selon leur taille décroissante.
%%%% Signature : remove_smaller_chains(Liste de sous-listes, Liste contenant les sous-listes non-incluses dans d'autres sous-listes).
remove_smaller_chains([],[]).
remove_smaller_chains([A], [A]).
remove_smaller_chains([[H1|T1], T1], [[H1|T1]]).
remove_smaller_chains([L1,L2], [L1,L2]) :- L1 \== L2.
remove_smaller_chains([H1,H2|T], Res) :-
	remove_smaller_chains([H2|T], [Res_H|Res_T]),
	remove_smaller_chains([H1,Res_H], Res_2), 
	% Alors, on s'explique. [Res_H|Res_T] correspond à ce qui a déjà été comparé/trié par récursion. 
	% Pour Res_T, on est sûr qu'il satisfait le prédicat remove_smaller_chains, car il a subi l'intégralité
	% du prédicat dans les récursions. Res_H peut être égal à H2. Il faut donc tester si Res_H est inclu dans H1.
	% On teste donc une nouvelle fois le prédicat, qui correspondra directement aux deux première def.
	% (Uniquement deux éléments). On obtient Res_2, qui contient soit [H1, Res_H] ou [H1] si Res_H € H1.
	% Il suffit ensuite de fusionner Res_2 et Res_T pour obtenir Res.
	append(Res_2, Res_T, Res)
	.

%%%% Appel successif des prédicats get_domino_chains pour récupérer l'intégralité des chaînes de dominos
%%%% jouables puis remove_smaller_chains pour supprimer les chaînes étant sous-chaîne d'une autre chaîne.
%%%% Signature : get_domino_unique_chains(Dominos de la main du joueur, Extrémité du board, Chaîne de dominos sans les sous-chaînes incluses)
get_domino_unique_chains(Player_hand, Chain_initiator, Chains) :-
	get_domino_chains(Player_hand, 0, Chain_initiator, Chains_with_subchains),
	remove_smaller_chains(Chains_with_subchains, Chains)
	.

%%%% Prédicats permettant de comparer deux dominos entre eux dans une liste triée par points décroissant.
sort_chains_by_size(>, A, B) :- 
	proper_length(A, Length_A), 
	proper_length(B, Length_B),
	Length_A >= Length_B
	.
sort_chains_by_size(<, A, B) :- 
	proper_length(A, Length_A), 
	proper_length(B, Length_B),
	Length_A < Length_B
	.
sort_chains_by_size(=, A, B) :- 
	proper_length(A, Length_A), 
	proper_length(B, Length_B),
	Length_A == Length_B
	.

%%%% Permet de récupérer le coup à jouer à partir d'une chaîne jouable de dominos et des extrémités du board.
%%%% Signature : get_move_from_chain(Chaine de domino jouable, 
%%%%								 Domino à une extrémité du board, 
%%%%								 Domino à l'autre extrémité du board, 
%%%%								 Coup à jouer)

get_move_from_chain([[0, Domino_rest]|_], [A1, A2], [_, _], [[A1, A2], [Domino_rest, 0]]).
get_move_from_chain([[Domino_rest, 0]|_], [A1, A2], [_, _], [[A1, A2], [Domino_rest, 0]]).
get_move_from_chain([[Domino_rest, 0]|_], [_, _], [B1, B2], [[B1, B2], [0, Domino_rest]]).
get_move_from_chain([[0, Domino_rest]|_], [_, _], [B1, B2], [[B1, B2], [0, Domino_rest]]).
get_move_from_chain([[A1, Domino_rest]|_], [A1, A2], [_, _], [[A1, A2], [Domino_rest, A1]]).
get_move_from_chain([[Domino_rest, A1]|_], [A1, A2], [_, _], [[A1, A2], [Domino_rest, A1]]).
get_move_from_chain([[Domino_rest, B2]|_], [_, _], [B1, B2], [[B1, B2], [B2, Domino_rest]]).
get_move_from_chain([[B2, Domino_rest]|_], [_, _], [B1, B2], [[B1, B2], [B2, Domino_rest]]).
get_move_from_chain([Domino|_], [0, A2], [_, _], [[0, A2], Domino]).
get_move_from_chain([Domino|_], [_, _], [B1, 0], [[B1, 0], Domino]).
get_move_from_chain([Domino|_], [A1, 0], [_, _], [[A1, 0], Domino]).
get_move_from_chain([Domino|_], [_, _], [0, B2], [[0, B2], Domino]).





%%%% Permet de choisir de ne jouer un joker qu'en dernier recours.
%%%% Si on peut jouer un coup sans utiliser de joker, on lance l'ia avec une main sans joker.
ia_finish_hand([Starter_domino|Board_rest], Player_hand, Move) :-
	hand_without_jokers(Player_hand, No_joker_hand),
	not(valid_moves([Starter_domino|Board_rest], No_joker_hand, [])),
	ia_with_clean_hand([Starter_domino|Board_rest], No_joker_hand, Move),
	!
	.
	
%%%% Sinon, on lance l'ia avec la main complète.
ia_finish_hand([Starter_domino|Board_rest], Player_hand, Move) :-
	ia_with_clean_hand([Starter_domino|Board_rest], Player_hand, Move),
	!.
	

ia_with_clean_hand([Starter_domino|Board_rest], Player_hand, Move) :-	
	% On récupère la première extrémité du board
	nth0(0, Starter_domino, Board_starter),
	% On récupère l'autre extrémité du board
	(last(Board_rest, End_domino);
	End_domino = Starter_domino),
	!,
	nth0(1, End_domino, Board_end),
	% On détermine les chaînes jouables à partir de la main du joueur
	% et des deux extrémités qu'on met dans Chains
	get_domino_unique_chains(Player_hand, Board_starter, Chains_from_starter),
	get_domino_unique_chains(Player_hand, Board_end, Chains_from_end),
	append(Chains_from_starter, Chains_from_end, Chains),
	% On trie ensuite les chaînes par taille
	predsort(sort_chains_by_size, Chains, Chains_sorted),
	% On récupère la première (i.e la plus petite) pour la jouer.
	nth0(0, Chains_sorted, Minimal_chain),
	reverse(Minimal_chain, Minimal_chain_reversed),
	get_move_from_chain(Minimal_chain_reversed, Starter_domino, End_domino, Move),
	!
	.
