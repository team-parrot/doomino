%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% Server POC

:- ensure_loaded('game.pl').

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).

:- http_handler(root(ping), ping, []).
:- http_handler(root(init), http_init, []).
:- http_handler(root(next), next, []).
:- http_handler(root(finished), finished, []).
:- http_handler(root(get_board), get_board, []).
:- http_handler(root(get_playerhand), get_playerhand, []).

game_initialized :-
    board(_).

server(Port) :-
    http_server(http_dispatch, [port(Port)]).

% /ping
ping(_Request) :-
    format('Access-Control-Allow-Origin: *~n'),
    format('Content-type: text/plain~n~n'),
    format('Pong !~n').

% /init?ia1=____&ia2=____
http_init(Request) :-
    format('Access-Control-Allow-Origin: *~n'),
    format('Content-type: text/plain~n~n'),
    http_parameters(Request, [
        ia1(IA1, []),
        ia2(IA2, [])
    ]),
    (
        init(IA1, IA2),
        writeln('Game initiated with:'),
        write('  - IA1: '), writeln(IA1),
        write('  - IA2: '), writeln(IA2)
    ,!;
        writeln("Can't find specified AI.")
    ).

% /next
next(_Request) :-
    format('Access-Control-Allow-Origin: *~n'),
    format('Content-type: text/plain~n~n'),
    board(Board),
    play_next(Board)
    ,!;
    writeln('Game has ended.').

play_next([]) :-
    play_first_move,!.
play_next(Board) :-
    \+gameover(Winner),
	move_history(History),
    length(History, LenHistory),
    (0 is mod(LenHistory, 2),
        ia1(IA1),
        play('Player 1', IA1)
    ,!;
        ia2(IA2),
        play('Player 2', IA2)
    ).

% /finished
finished(_Request) :-
    format('Access-Control-Allow-Origin: *~n'),
    format('Content-type: text/plain~n~n'),
    (gameover(Winner),
        writeln(Winner)
    ,!;
        writeln('-')
    ).

% /get_board
get_board(_Request) :-
    format('Access-Control-Allow-Origin: *~n'),
    format('Content-type: text/plain~n~n'),
    (game_initialized,
        board(Board)
    ,!;
        Board = []
    ),
    writeln(Board).

% /get_playerhand
get_playerhand(Request) :-
    format('Access-Control-Allow-Origin: *~n'),
    format('Content-type: text/plain~n~n'),
    http_parameters(Request, [
        player(PlayerNumber, [])
    ]),
    (PlayerNumber = '1',
        player1(Player)
    ,!;
        player2(Player)
    ),
    writeln(Player).

% Lance le serveur sur le port 5000
:- server(5001).




