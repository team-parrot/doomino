:- ensure_loaded("domino_utils.pl").
:- ensure_loaded("board_utils.pl").
:- ensure_loaded("hand_utils.pl").

%%% Jouer en tant qu'IA :

ia_win_by_points(Board, Player_hand, Move) :- 
	predsort(domino_sort_points_desc, Player_hand, Sorted_hand), 
	get_move_from_max(Board, Sorted_hand, Move),
	!.

get_move_from_max([H_board|T_board], [[A,B]|_], [H_board, [A,B]]) :- 
	valid_board([[A,B]|[H_board|T_board]]).
	
get_move_from_max([H_board|T_board], [[A,B]|_], [H_board, [B,A]]) :- 
	valid_board([[B,A]|[H_board|T_board]]).
	
get_move_from_max(Board, [[A,B]|_], Move) :- 
	append(Board, [[A,B]], Appended_list), 
	valid_board(Appended_list), 
	last(Board, Last_domino), 
	Move = [Last_domino, [A,B]]
	.
	
get_move_from_max(Board, [[A,B]|_], Move) :- 
	append(Board, [[B,A]], Appended_list), 
	valid_board(Appended_list), 
	last(Board, Last_domino), 
	Move = [Last_domino, [B,A]]
	.
	
get_move_from_max(Board, [_|T], Move) :- 
	get_move_from_max(Board, T, Move).