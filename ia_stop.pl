%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% IA block v2

:- ensure_loaded(domino_utils).
:- ensure_loaded(hand_utils).
:- ensure_loaded(ia_block_utils).

ia_stop(Board, PlayerHand, Move) :-
    move_history(MoveHistory), length(MoveHistory, NbMoves),
    (0 is mod(NbMoves, 2),
        Player = 'Player 1'
    ,!;
        Player = 'Player 2'
    ),
    move_history(History), write('Hist : '), writeln(History),
    write('Mon jeu : '), writeln(PlayerHand),
    predict_adv_hand(Player, Board, PlayerHand, AdvHand ),
    valid_moves(Board, PlayerHand, Moves ),
    valid_moves(Board, AdvHand, AdvMoves ),
    write("Je peux : "), writeln(Moves),
    write("Il peut : "), writeln(AdvMoves),
    %trace,
    findall([NbRemTurns, TryMove], (
        member(TryMove, Moves),
        write("Si je joue : "), writeln(TryMove),
        %trace,
        play_move(Board, TryMove, NewBoard),
        other_player(Player, NextPlayer),
        %ia_stop_player_hand(NextPlayer, NextPlayerHand),
        [_, TryPiece] = TryMove,
        ia_stop_withdraw_piece(TryPiece, PlayerHand, NewPlayerHand),
        predict_adv_hand(Player, NewBoard, NewPlayerHand, NextPlayerHand),
        ia_stop_rem_turns(NextPlayer, NewBoard, NextPlayerHand, NbRemTurns),
        write("  Il lui reste "), write(NbRemTurns), writeln(" coups à jouer."),
        !
        %(1 is mod(NbRemTurns, 2),
        %    writeln('Ok, je vais pas être suicidaire...'),
        %    !%,fail
        %; true)
    ), Possibilities ),
    sort(Possibilities, SortedPossibilities),
    write("Voici ce que je peux faire : "), writeln(SortedPossibilities),
    nth0(0, SortedPossibilities, ChoosenPossibility), % Fails if there are no moves.
    nth0(1, ChoosenPossibility, Move).

ia_stop_rem_turns(_, _, [], 0).
ia_stop_rem_turns(Player, Board, PlayerHand, MaxRemTurns) :-
    %%%write('Le jeu de '), write(Player), write(' : '), writeln(PlayerHand),
    predict_adv_hand(Player, Board, PlayerHand, AdvHand ),
    valid_moves(Board, PlayerHand, Moves ),
    valid_moves(Board, AdvHand, AdvMoves ),
    length(AdvMoves, AdvMovesLen),
    (AdvMovesLen = 0,
        MaxRemTurns = 0
    ,!;
        %%%write("    Je peux : "), writeln(Moves),
        %%%write("    Il peut : "), writeln(AdvMoves),
        findall(RemTurns, (
            member(TryMove, Moves),
            %%%write("    Si je joue : "), writeln(TryMove),
            play_move(Board, TryMove, NewBoard),
            [_, TryPiece] = TryMove,
            ia_stop_withdraw_piece(TryPiece, PlayerHand, NewPlayerHand),
            other_player(Player, NextPlayer),
            predict_adv_hand(Player, NewBoard, NewPlayerHand, NextPlayerHand),
            ia_stop_rem_turns(NextPlayer, NewBoard, NewPlayerHand, RemTurnsAfter),
            RemTurns is RemTurnsAfter + 1,
            move_history(MoveHistory),
            length(MoveHistory, MoveHistoryLen),
            length(Board, BoardLen),
            DiffLen is BoardLen - MoveHistoryLen,
            (0 is mod(DiffLen, 2), RemTurns = 1,
                !
            ;
                1 is mod(DiffLen, 2), RemTurns = 0,
                !
            ; true
            ),!
        ), RemTurnsList ),
        length(RemTurnsList, RemTurnsListLen),
        (RemTurnsListLen = 0,
            MaxRemTurns = 0
        ,!;
            sort(RemTurnsList, SortedRemTurnsList),
            %trace,
            %%%write("   Voici la liste des nb de coups restants : "), writeln(SortedRemTurnsList),
            nth0(0, SortedRemTurnsList, MaxRemTurns)
        )
    ).

ia_stop_withdraw_piece([P1,P2], PlayerHand, NewPlayerHand) :-
    delete(PlayerHand, [P1,P2], NewPlayerHand_tmp),
    delete(NewPlayerHand_tmp, [P2,P1], NewPlayerHand).

ia_stop_player_hand('Player 1', Player1Hand) :-
    player1(Player1Hand).
ia_stop_player_hand('Player 2', Player2Hand) :-
    player2(Player2Hand).




