%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% Match runner

:- ensure_loaded('game.pl').
:- ensure_loaded('utils.pl').

match(Times, IA1, IA2, Results) :-
    hide_output,
    match_exec(Times, IA1, IA2, ResultList),
    reduce_match_results(ResultList, Results),
    restore_output.

% In:
% [1, 1, 2, 1, 2]
%
% Out:
% [0, 3, 2]
reduce_match_results([], [0, 0, 0]).
reduce_match_results([R | ResultList], [NR0, NR1, NR2]) :-
    reduce_match_results(ResultList, [R0, R1, R2]),
    (R = 1,
        NR0 is R0, NR1 is R1 + 1, NR2 is R2
    ,!;
     R = 2,
        NR0 is R0, NR1 is R1, NR2 is R2 + 1
    ,!;
        NR0 is R0 + 1, NR1 is R1, NR2 is R2
    ).

match_exec(0    ,   _,   _, []) :- !.
match_exec(Times, IA1, IA2, [Result | OtherResults]) :-
    launch(IA1,IA2),
    gameover(Winner),
    (Winner = 'Player 1',
        Result = 1
    ,!;
        Winner = 'Player 2',
        Result = 2
    ,!;
        Result = 0
    ),
    NewTimes is Times - 1,
    match_exec(NewTimes, IA1, IA2, OtherResults).

