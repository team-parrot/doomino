# Doomino - ALIA

Doomino est le projet d'ALIA de la Team Parrot. Il s'agit d'un jeu de domino
codé en ProLog.

Ce projet a été réalisé par la Team Parrot (H4304), 4IF 2017, INSA Lyon.
Membres de la Team Parrot :
- Tom Bourret
- Piotr Czajkowski
- Jean Debard
- Kévin Dumanoir
- Vincent Guillon
- Thomas Lacroix
- Renaud Meurisse.

## Contexte, règles du jeu

- Deux joueurs disposent d'une main de dominos. Le nombre est à définir en début de partie.
- Le joueur avec le domino le plus fort en points (somme des deux nombres du domino) commence la partie.
- La suite des dominos forme une chaîne.
- Lors de son tour, le joueur **doit** placer un domino ou alors piocher un nouveau domino. Il peut placer le domino au début ou à la fin de la chaîne.
- La partie se termine lorsqu'un des joueurs a une main vide **ou** que la partie est bloquée (aucune combinaison possible pour aucun joueur, pioche vide)

Le but est de réaliser la version simple du jeu de domino (tous les dominos se suivent un à un).
On pourra réaliser *si le temps nous le permet* une variante où on peut placer une nouvelle chaîne à partir d'un domino double.

## Comment utiliser Doomino

Il est possible de lancer le jeu de deux manières différentes : en console et en
graphique dans un navigateur Web.

### En console

La sortie console est privilégiée pour suivre les IA au tour à tour (elle est
plus pratique pour ça), et est l'unique méthode permettant de lancer un grand
nombre de matchs pour obtenir des statistiques de perfommances IA contre IA.

Dans tous les cas, le fichier Prolog à exécuter est `match.pl` :
```shell
swipl -f match.pl
```

Pour lancer un match, il faut utiliser le prédicat `launch/2`, en précisant les
IA que l'on souhaite se faire affronter :
```prolog
:- launch(ia_1, ia_2).
```

Avec `ia_1` et `ia_2` pouvant être :
- ia_random
- ia_win_by_points
- ia_block
- ia_finish_hand
- ia_stop

Pour lancer plusieurs matchs, il faut utiliser le prédicat `match/4`, en précisant les
IA que l'on souhaite se faire affronter :
```prolog
:- match(nombre_de_matches, ia_1, ia_2, Resultats).
Resultats = [nombre_egalites, nombre_victoires_J1, nombre_victoires_J2].
```

### Dans un navigateur Web

La sortie Web est plus agréable à utiliser pour visualiser des affrontements.
Pour la lancer, il faut lancer le serveur Prolog ainsi que l'application Web.

Pour lancer le serveur Prolog :

```shell
swipl -f server.pl
```

L'application Web est dans le dépôt [doomino-webclient](https://gitlab.com/team-parrot/doomino-webclient),
il faut suivre les instructions qui y sont indiquées, à savoir :
```shell
git clone https://gitlab.com/team-parrot/doomino-webclient.git
cd doomino-webclient
npm install
npm run production
```

Puis ouvrir le fichier `index.html` dans un navigateur.

## Comportements des IA

Nous avons retenu 5 comportements intéressants pour développer des IA:
- random : joue un coup aléatoire parmi tous les coups possibles.
- finish_hand : joue les dominos qui ne font pas partie d’une chaîne, afin de se
                débarrasser des dominos pouvant potentiellement le bloquer.
- win_by_points : joue toujours son domino avec le plus de points.
- block : prédit la main de l’adversaire dans le pire des cas et joue le coup
          qui lui laisse le moins de possibilités de coup.
- stop : prédit toutes les possibilités et joue le coup qui l’amène à la victoire
         le plus rapidement.

## Répartition de l'équipe

Il existe quatre pôles où sont répartis les membres de l'équipe:

- Affichage: Représentation en forme console du jeu (graphique via HTML/JS ?), interactions homme/machine (Vincent)
- Logique de jeu : gestion d'un tour, implémentation des règles, Gestion du gameover
- IA 1 : Une moitié de la charge de travail liée aux IAs
- IA 2 : l'autre moitié

## Aide à la contribution sur GitLab

Afin de simplifier le plus possible la collaboration entre membres sur GitLab, **chaque groupe de travail est invité à utiliser une branche du projet.**

**Nom de la branche : `nompôle_dev`**

Afin de fusionner les différents développements, nous utiliserons les fonctionnalités de pull request de GitLab pour effectuer les merges.
L'utilisation de `git rebase` et du suffixe `_dev` sera détaillée ultérieurement.