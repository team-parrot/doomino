%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% General utils

:- dynamic previous_out/1.
:- dynamic current_out/1.

hide_output :-
    retractall(previous_out(_)),
    retractall(current_out(_)),
    current_output(Orig),
    open('/dev/null', write, Out),
    %open('NUL', write, Out),
    set_output(Out),
    assert(previous_out(Orig)),
    assert(current_out(Out)).

restore_output :-
    previous_out(Orig),
    current_out(Out),
    close(Out),
    set_output(Orig),
    retractall(previous_out(_)),
    retractall(current_out(_)).

