distribute_hand(HAND,DECK):- distribute_hand(HAND,DECK,7).
distribute_hand([],_,0).
distribute_hand([H|T1],[H|T2],VAL):- N is VAL-1, distribute_hand(T1,T2,N).

remove(L1,L2,NB):- length(P,NB), append(P,L2,L1).

distribute(HAND1, HAND2, DECK, NEWDECK) :- distribute_hand(HAND1,DECK), remove(DECK,X,7), distribute_hand(HAND2,X), remove(X,NEWDECK,7).

draw(HAND,[],NEWHAND,NEWDECK):-NEWHAND = HAND, NEWDECK = [],!.
draw(HAND,[H|T],NEWHAND,NEWDECK) :- append([H],HAND,NEWHAND), append(T,[],NEWDECK).
