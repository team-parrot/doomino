%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% Board utils

%%%% Verifier si un plateau est valide
valid_board([]).
valid_board([ [_, _] ]).
valid_board([ [_,0],[B,C]|X ]) :- valid_board([ [B,C]|X ]).
valid_board([ [_,_],[0,C]|X ]) :- valid_board([ [0,C]|X ]).
valid_board([ [_,B],[B,C]|X ]) :- valid_board([ [B,C]|X ]).

%%%% Renverse un plateau, en retournant chacune de ses pieces. Le plateau
%%%% renverse est donc equivalent au plateau d'origine.
reverse_board([], []).
reverse_board(Board, ReversedBoard) :-
    reverse(Board, Reversed), reverse_each(Reversed, ReversedBoard).

%%%% Renverse chaque piece d'un plateau.
reverse_each([], []).
reverse_each([[P1,P2]|B], [[P2,P1]|RB]) :-
    reverse_each(B, RB).

