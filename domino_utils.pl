%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% Domino utils

%%%% V�rifier si deux dominos sont �gaux
domino_are_equal([A, B], [A, B]).

%%%% Pr�dicat v�rifiant si deux dominos sont acollables.
domino_can_chain([_, B], [B, _]).
domino_can_chain([A, _], [A, _]).
domino_can_chain([_, B], [_, B]).
domino_can_chain([A, _], [_, A]).

%%%% V�rifie si un nombre est inclu dans le domino (Eventuellement penser � v�rifier que A est un nombre non ?)
domino_get_numbers([0, B], _, B).
domino_get_numbers([A, 0], _, A).
domino_get_numbers([A, _], 0, A).
domino_get_numbers([_, B], 0, B).
domino_get_numbers([A, B], A, B).
domino_get_numbers([A, B], B, A).


%%%% Pr�dicats permettant de comparer deux dominos entre eux dans une liste tri�e par points d�croissant.
domino_sort_points_desc(>, [A1, A2], [B1, B2]) :- A1+A2 < B1+B2.
domino_sort_points_desc(<, [A1, A2], [B1, B2]) :- A1+A2 >= B1+B2.
% Utilisation de >= car on souhaite conserver les doublons.
% Par d�faut les m�thodes commes predsort suppriment les doublons.
domino_sort_points_desc(=, [A1, A2], [B1, B2]) :- A1+A2 == B1+B2.

% Exemple d'utilisation permettant de trier une liste de dominos :
% ?- predsort(domino_sort_points_desc, [[1, 2], [0, 1], [1, 0], [3, 0], [1,5]], X).

