cartesian(L1,L2):- cartesian(L1,L1,L2).
cartesian(_, [], []).
cartesian([], [_|T1], L2):- cartesian(T1, T1, L2).
cartesian([H1|T1], [H2|T2], [[H1,H2]|T3]):-cartesian(T1, [H2|T2], T3).

create_deck(SET) :- findall(X,between(0,6,X),L),cartesian(L,L2),random_permutation(L2, SET).
