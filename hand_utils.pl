%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% Hand utils

:- [board_utils].

reverse_moves([], []).
reverse_moves([M|Ms], [RM|RMs]) :-
    reverse_each(M, RM), reverse_moves(Ms, RMs).

%%%% Retourne tous les coups possibles
%%%% valid_move( Board, Hand, Move=[NextTo, Played] ).
valid_moves( Board, PlayerHand, Moves ) :-
    findall( M, valid_move_begin( Board, PlayerHand, M), Moves_begin ),
    reverse_board( Board, ReversedBoard ),
    findall( M, valid_move_begin( ReversedBoard, PlayerHand, M), Moves_end_reversed ),
    reverse_moves( Moves_end_reversed, Moves_end ),
    append([Moves_begin, Moves_end], Moves),
    !.

%%%% Ajout de domino au début
valid_move_begin([[0,B]|_],[[P1,P2]|_],[[0,B],[P1,P2]]).
valid_move_begin([[0,B]|_],[[P1,P2]|_],[[0,B],[P2,P1]]).
valid_move_begin([H|_], [[A,0]|_],[H,[A,0]]).
valid_move_begin([H|_], [[0,B]|_],[H,[B,0]]).
valid_move_begin( [[B1,B2]|B], [[P1,P2]|Hs], Move ) :-
    (P1 is B1, Move = [[B1,B2], [P2,P1]] ; P2 is B1, Move = [[B1,B2], [P1,P2]]);
    valid_move_begin([[B1,B2]|B], Hs, Move).
    
%%%% Retourne tous les coups possibles sans les jokers
%%%% valid_move_without_jokers( Board, Hand, Move=[NextTo, Played] ).
%valid_moves_without_jokers( Board, PlayerHand, Moves ) :-
%    findall( M, valid_move_without_jokers_begin( Board, PlayerHand, M), Moves_begin ),
%    reverse_board( Board, ReversedBoard ),
%    findall( M, valid_move_without_jokers_begin( ReversedBoard, PlayerHand, M), Moves_end_reversed ),
%    reverse_moves( Moves_end_reversed, Moves_end ),
%    append([Moves_begin, Moves_end], Moves).

%%%% Ajout de domino au début sans considérer les jokers dans la main du joueur
%valid_move_without_jokers_begin([[0,B]|_],[[P1,P2]|_],[[0,B],[P1,P2]]).
%valid_move_without_jokers_begin([[0,B]|_],[[P1,P2]|_],[[0,B],[P2,P1]]).
%valid_move_without_jokers_begin( [[B1,B2]|B], [[P1,P2]|Hs], Move ) :-
%    (P1 is B1, Move = [[B1,B2], [P2,P1]] ; P2 is B1, Move = [[B1,B2], [P1,P2]]);
%    valid_move_without_jokers_begin([[B1,B2]|B], Hs, Move).

