%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
%% IA block

:- ensure_loaded(domino_utils).
:- ensure_loaded(hand_utils).
:- ensure_loaded(ia_block_utils).

ia_block(Board, PlayerHand, Move) :-
    move_history(History), write('Hist : '), writeln(History),
    write('Mon jeu : '), writeln(PlayerHand),
    predict_adv_hand( Board, PlayerHand, AdvHand ),
    valid_moves( Board, PlayerHand, Moves ),
    valid_moves( Board, AdvHand, AdvMoves ),
    write("Je peux : "), writeln(Moves),
    write("Il peut : "), writeln(AdvMoves),
    findall( [NbRemMoves, TryMove], (
        member(TryMove, Moves),
        write("Si je joue : "), writeln(TryMove),
        play_move(Board, TryMove, NewBoard),
        valid_moves(NewBoard, AdvHand, RemMoves), length(RemMoves, NbRemMoves),
        write("  Il lui reste "), write(NbRemMoves), writeln(" coups possibles.")
    ), Possibilities ),
    sort( Possibilities, SortedPossibilities),
    write("Voici ce que je peux faire : "), writeln(SortedPossibilities),
    nth0(0, SortedPossibilities, ChoosenPossibility), % Fails if there are no moves.
    nth0(1, ChoosenPossibility, Move).

% Prédicats temporaires, à mettre dans les utils / utiliser ceux qui existent.
other_player('Player 1', 'Player 2').
other_player('Player 2', 'Player 1').

%%%% Prédiction des pièces possibles dans la main de l'aversaire en fonction
%%%% de la première pièce, de ses propres pièces et de celles du plateau
predict_adv_hand( Board, PlayerHand, AdvHand ) :-
    findall( [I,J], (
        between(0, 6, I), between(0, I, J),
        \+member([I,J], Board), \+member([I,J], PlayerHand),
        \+member([J,I], Board), \+member([J,I], PlayerHand)
    ), AdvHand_raw ),
    filter_first_piece( AdvHand_raw, AdvHand, =< ),
    write("Jeu de l'adv : "), writeln(AdvHand).

%%%% Prédiction de la pioche en fonction de la première pièce,
%%%% de ses propres pièces et de celles du plateau
%handle_stock( Player, Board, PlayerHand, Stock ) :-
%    (
%        % La pioche n'a pas encore été prédite
%        iap_current_turn(Player, Turn), Turn < 3,
%        findall( [I,J], (
%            between(0, 6, I), between(0, I, J),
%            \+member([I,J], Board), \+member([I,J], PlayerHand),
%            \+member([J,I], Board), \+member([J,I], PlayerHand)
%        ), Stock_raw ),
%        filter_first_piece( Player, Stock_raw, Stock, >= ),
%        assert(iap_stock(Player, Stock)),
%    !;
%        % Màj de la pioche
%        true
%    ).

domino_comp_points([A1, A2], <, [B1, B2]) :- A1+A2 < B1+B2.
domino_comp_points([A1, A2], >, [B1, B2]) :- A1+A2 > B1+B2.
domino_comp_points([A1, A2], =<, [B1, B2]) :- A1+A2 =< B1+B2.
domino_comp_points([A1, A2], >=, [B1, B2]) :- A1+A2 >= B1+B2.
domino_comp_points([A1, A2], ==, [B1, B2]) :- A1+A2 == B1+B2.
domino_comp_points([A1, A2], \==, [B1, B2]) :- A1+A2 \== B1+B2.

%%%% Filtre une liste (main ou plateau) en connaissance de
%%%% la première pièce jouée par l'adversaire, le cas échéant.
filter_first_piece( Unfiltered, Filtered, Comp ) :-
    % Seul le joueur 2 peut filtrer les pièces avec la première jouée
    move_history(MoveHistory),
    length(MoveHistory, MoveHistoryLen),
    1 is mod(MoveHistoryLen, 2),
    % On ne peut plus prédire si joueur 1 a pioché
    \+player1_drawn(MoveHistory),
    % Affichage de la mémoire
    nth0(0, MoveHistory, FirstMove), last(FirstMove, FirstPiece),
    write("La pièce max de l'adv : "), write(FirstPiece), writeln("."),
    % Filtre les pièces meilleures que la premiere pièce
    findall( [I,J], (
        member([I,J], Unfiltered),
        domino_comp_points([I,J], Comp, FirstPiece)
    ), Filtered ),
    !; Filtered = Unfiltered.

player1_drawn([[]   | MoveHistory]).
player1_drawn([_, _ | MoveHistory]) :-
    player1_drawn(MoveHistory).






























