%% Copyright (C) 2017 - Team Parrot, INSA Lyon
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%%
% Game

:- [board_utils].
:- [domino_utils].
:- [ia_random].
:- [ia_block].
:- [ia_finish_hand].
:- [ia_win_by_points].
:- [ia_stop].
:- [deck].
:- [distribute].

:- dynamic board/1.
:- dynamic deck/1.
:- dynamic player1/1.
:- dynamic player2/1.
:- dynamic move_history/1.
:- dynamic ia1/1.
:- dynamic ia2/1.

%ia(Strategy, Board, PlayerHand, Move).

display_board(Board):-
    writeln('---------------------------------'),
    writeln('Board :'),
    writeln(Board),
    writeln('---------------------------------').

choose_hand('Player 1', PlayerHand, AdversaryHand):-
       player1(PlayerHand),player2(AdversaryHand).
choose_hand('Player 2', PlayerHand, AdversaryHand):-
    player2(PlayerHand),player1(AdversaryHand).

withdraw_piece('Player 1', [P1,P2], PlayerHand):- 
    subtract(PlayerHand,[[P1,P2],[P2,P1]],NewPH),
    retract(player1(PlayerHand)),
    assert(player1(NewPH)).
withdraw_piece('Player 2', [P1,P2], PlayerHand):-
    subtract(PlayerHand,[[P1,P2],[P2,P1]],NewPH), 
    retract(player2(PlayerHand)),
    assert(player2(NewPH)).

play_move( Board, [[A,B],[B,C]], NewBoard ) :-
    last(Board, [A,B]),
    append(Board, [[B,C]], NewBoard),
    !.
play_move( Board, [[A,B],[C,A]], NewBoard ) :-
    nth0(0, Board, [A,B]),
    append([[C,A]], Board, NewBoard),
    !.
play_move( Board, [[A,B], [0,C]], NewBoard ) :-
	last(Board, [A,B]),
	append(Board, [[0,C]], NewBoard),
    !.
play_move( Board, [[A,B], [C,0]], NewBoard ) :-
	nth0(0, Board, [A,B]),
	append([[C,0]], Board, NewBoard),
    !.
play_move( Board, [[0,B], [C,D]], NewBoard ) :-
	nth0(0, Board, [0,B]),
	append([[C,D]], Board, NewBoard),
    !.
play_move( Board, [[A,0], [C,D]], NewBoard ) :-
	last(Board,[A,0]),
	append(Board, [[C,D]], NewBoard),
    !.

add_move_history(Move) :-
    move_history(History),
    append(History, [Move], NewHistory),
    retract(move_history(History)),
    assert(move_history(NewHistory)).

apply_move(Board, NewBoard):-retract(board(Board)),assert(board(NewBoard)).

change_player('Player 1', 'Player 2').
change_player('Player 2', 'Player 1').

change_ia('Player 1', IA):- ia1(IA).
change_ia('Player 2', IA):- ia2(IA).

best_piece(PlayerHand, BestPiece):-
    predsort(domino_sort_points_desc, PlayerHand, [H|T]),
    BestPiece = H.

gameover(Winner):-
	(player1(Player1Hand),
		Player1Hand = [],
		Winner = 'Player 1'
	,!;
	player2(Player2Hand),
		Player2Hand = [],
		Winner = 'Player 2'
	).
gameover(Winner):-
	board(Board),
	player1(Player1),
	player2(Player2),
	valid_moves(Board,Player1,[]),
	valid_moves(Board,Player2,[]),
	deck([]),
	hand_value(Player1, Total1),
	hand_value(Player2, Total2),
	(Total1>Total2,
		Winner='Player 2'
	,!;
		(Total1<Total2,
			Winner='Player 1'
		,!;
			Winner='No one'
		)
	).
	
hand_value([], 0).
hand_value([[H1,H2]|T], Total):- hand_value(T,X), Total is X+H1+H2.

launch(IA1,IA2) :-
    init(IA1,IA2),
    play_first_move,
    game_loop('Player 2', IA2).

game_loop(Player, IA) :-
    play(Player, IA),
    change_player(Player, NextPlayer),
	change_ia(NextPlayer, NewIA),
    (gameover(Winner),
        write(Winner), writeln(' has won !'),
        !;
        game_loop(NextPlayer, NewIA)
    ).

init(IA1,IA2):-
	retractall(player1(_)),
	retractall(player2(_)),
	retractall(board(_)),
	retractall(deck(_)),
	retractall(move_history(_)),
	retractall(ia1(_)),
	retractall(ia2(_)),
    writeln('Starting game...'),
    create_deck(Deck),
    distribute(Player1, Player2, Deck, NewDeck),
    assert(deck(NewDeck)),
    assert(player1(Player1)),
    assert(player2(Player2)),
    assert(board([])),
	assert(ia1(IA1)),
	assert(ia2(IA2)),
    !,
    writeln('Done!').

pause :-
    trace, writeln('Pause...'), notrace.

play_first_move :-
    player1(Player1),
    best_piece(Player1, BestPiece),
    retract(board([])),
    assert(board([BestPiece])),
    withdraw_piece('Player 1', BestPiece, Player1),
    assert(move_history([[[], BestPiece]])),
    writeln('Player 1 played.'),
    board(Board),
    display_board(Board),
    !.

play(Player, IA):-
    writeln('############################################################'),
    write(Player),
    writeln('\'s turn :'),
    board(Board),
    choose_hand(Player, PlayerHand, AdversaryHand),
    (
		(
			IA = 'ia_random',
			ia_random(Board, PlayerHand, [MovePlace,MovePiece]),
			!;
			IA = 'ia_block', 
			ia_block(Board, PlayerHand, [MovePlace,MovePiece]),
			!;
			IA = 'ia_stop', 
			ia_stop(Board, PlayerHand, [MovePlace,MovePiece]),
			!;
			IA = 'ia_finish_hand',
			ia_finish_hand(Board, PlayerHand, [MovePlace,MovePiece]),
			!;
			IA = 'ia_win_by_points',
			ia_win_by_points(Board, PlayerHand, [MovePlace,MovePiece])
		),
    	write(Player), write(' plays: '), writeln([MovePlace,MovePiece]),
    	play_move(Board, [MovePlace,MovePiece], NewBoard),
    	withdraw_piece(Player, MovePiece, PlayerHand),
    	add_move_history([MovePlace,MovePiece]),
    	apply_move(Board, NewBoard),
		display_board(NewBoard),
	!;
		deck(Deck),
        write(Player),writeln(' draws :'),
		writeln(PlayerHand),
		(draw(PlayerHand,Deck,NewPlayerHand,NewDeck),
		!;
			true
		),
		retract(deck(Deck)),
		assert(deck(NewDeck)),
		(Player = 'Player 1',
			retract(player1(PlayerHand)),
			assert(player1(NewPlayerHand))
		,!;
			retract(player2(PlayerHand)),
			assert(player2(NewPlayerHand))
		),
		writeln(NewPlayerHand),
        add_move_history([]),
		display_board(Board)
	).

